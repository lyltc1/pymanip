"""
Some experimental codes related to trimesh manipulation.

"""

# 15 DECEMBER 2016
import openravepy as orpy
env = orpy.Environment()
env.SetViewer('qtcoin')
collisionchecker = orpy.RaveCreateCollisionChecker(env, 'ode')
env.SetCollisionChecker(collisionchecker)

from pymanip.planningutils import utils
import numpy as np
import random
rng = random.SystemRandom()

mobj = env.ReadKinBodyXMLFile('../models/table1.dae')
env.Add(mobj)
tm = env.Triangulate(mobj)

def VisualizeTriangles(orkinbody):
    """Visualize triangles in a trimesh of the given kinbody. Colors are randomly sampled.

    Return a list of KinBodies. Each kinbody is a trimesh containing one triangle.
    """
    mesh = orkinbody.GetEnv().Triangulate(orkinbody)
    faces = []
    for (iface, face) in enumerate(mesh.indices):
        v0 = mesh.vertices[face[0]]
        v1 = mesh.vertices[face[1]]
        v2 = mesh.vertices[face[2]]
        indices = np.array([[0, 1, 2]])

        face_trimesh = orpy.TriMesh([v0, v1, v2], indices)
        face_kinbody = orpy.RaveCreateKinBody(env, '')
        face_kinbody.InitFromTrimesh(face_trimesh)
        face_kinbody.SetName("face{0:04d}".format(iface))
        color = np.asarray([rng.random() for _ in xrange(3)])
        for l in face_kinbody.GetLinks():
            for g in l.GetGeometries():
                g.SetDiffuseColor(color)
        env.Add(face_kinbody)
        faces.append(face_kinbody)

    return faces


import trimesh as tm
testmesh = tm.load_mesh(file_obj='../models/table1.stl', file_type='stl')
testmesh.fix_normals()



def VisualizeFacets(mesh):
    """Assign one facet to one color. Facet information is obtained from a trimesh object
    (python.trimesh package)

    """
    facets = []
    mesh_facets = mesh.facets()
    used_faces = []
    for (ifacet, facet) in enumerate(mesh_facets):
        faces = []
        uniqueIndices = []
        for iface in facet:
            faces.append(mesh.faces[iface])
            uniqueIndices += mesh.faces[iface].tolist()
            used_faces.append(iface)
        uniqueIndices = list(set(uniqueIndices))
        vertices = [mesh.vertices[i] for i in uniqueIndices]

        # Reindex the indices
        newIndices = []
        for face in faces:
            newFace = [uniqueIndices.index(index) for index in face]
            newIndices.append(newFace)
            
        facet_trimesh = orpy.TriMesh(vertices, newIndices)
        facet = orpy.RaveCreateKinBody(env, '')
        facet.InitFromTrimesh(facet_trimesh)
        facet.SetName('facet{0:02d}'.format(ifacet))
        color = np.asarray([rng.random() for _ in xrange(3)])
        for l in facet.GetLinks():
            for g in l.GetGeometries():
                g.SetDiffuseColor(color)
        env.Add(facet)
        facets.append(facet)

    nfacets = len(mesh_facets)
    for iface in xrange(len(mesh.faces)):
        if iface in used_faces:
            continue

        vertices = [mesh.vertices[i] for i in mesh.faces[iface]]
        face_trimesh = orpy.TriMesh(vertices, [[0, 1, 2]])
        face = orpy.RaveCreateKinBody(env, '')
        face.InitFromTrimesh(face_trimesh)
        face.SetName('facet{0:02d}'.format(nfacets))
        nfacets += 1
        color = np.asarray([rng.random() for _ in xrange(3)])
        for l in face.GetLinks():
            for g in l.GetGeometries():
                g.SetDiffuseColor(color)
        env.Add(face)
        facets.append(face)
    
    return facets

################################################################################
import openravepy as orpy
env = orpy.Environment()
env.SetViewer('qtcoin')
collisionchecker = orpy.RaveCreateCollisionChecker(env, 'ode')
env.SetCollisionChecker(collisionchecker)

import numpy as np
import random
rng = random.SystemRandom()
from pymanip.planningutils import utils, myobject

mobj = env.ReadKinBodyXMLFile('/home/puttichai/puttichai/checkoutroot/pymanip/pymanip/models/chair.xml')
env.Add(mobj)
myObject = myobject.MyObject(mobj)

floor = orpy.RaveCreateKinBody(env, '')
floor.InitFromBoxes(np.array([[0.0, 0.0, -0.025, 3.0, 3.0, 0.025]]))
floor.SetName('floor')
floor.GetLinks()[0].GetGeometries()[0].SetDiffuseColor([1.0, 0.5, 0.0])
floor.GetLinks()[0].GetGeometries()[0].SetAmbientColor([0.6, 0.6, 0.6])
env.Add(floor)

class OpenRAVEKinBodyWrapper(object):
     def __init__(self, mobj, TcomLocal):
         self.mobj = mobj
         self.Tcom = np.array(TcomLocal)
         self.TcomInv = np.linalg.inv(self.Tcom)
 
     def GetEnv(self):
         return self.mobj.GetEnv()
 
 
     def GetTransform(self):
         return np.dot(self.mobj.GetTransform(), self.Tcom)
 
 
     def SetTransform(self, T):
         self.mobj.SetTransform(np.dot(T, self.TcomInv))

orwrapper = OpenRAVEKinBodyWrapper(mobj, myObject.Tcom)

#-------------------

import trimesh as tm

mobj = env.ReadKinBodyXMLFile('/home/puttichai/puttichai/checkoutroot/pymanip/pymanip/models/ikea_right_frame.kinbody.xml')
env.Add(mobj)

mesh = env.Triangulate(mobj)
testmesh = tm.Trimesh(vertices=mesh.vertices, faces=mesh.indices)
meshes = testmesh.split(only_watertight=False)

bodies = []
for (imesh, mesh) in enumerate(meshes):
    body_mesh = orpy.TriMesh(mesh.vertices, mesh.faces)
    body = orpy.RaveCreateKinBody(env, '')
    body.InitFromTrimesh(body_mesh)
    body.SetName('l{0:02d}'.format(imesh))
    for l in body.GetLinks():
        for g in l.GetGeometries():
            color = np.asarray([rng.random() for _ in xrange(3)])
            g.SetDiffuseColor(color)
    env.Add(body)
    bodies.append(body)

#------------------

def tm2or(mesh, name='default', color=[]):
    ormesh = orpy.TriMesh(mesh.vertices, mesh.faces)
    body = orpy.RaveCreateKinBody(env, '')
    body.InitFromTrimesh(ormesh)
    body.SetName(name)
    if len(color) == 0:
        color = np.asarray([rng.random() for _ in xrange(3)])
    for l in body.GetLinks():
        for g in l.GetGeometries():
            g.SetDiffuseColor(color)
    env.Add(body, True)
    return body
