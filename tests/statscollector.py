import openravepy as orpy
import numpy as np
import time
import pickle
from os import path

from pymanip.planningutils import utils, myobject

## Environment initialization
setviewer = 0
viewername = 'qtosg'
collisioncheckername = 'fcl_'
env = orpy.Environment()
cc = orpy.RaveCreateCollisionChecker(env, collisioncheckername)
cc.SetCollisionOptions(orpy.CollisionOptions.ActiveDOFs)
env.SetCollisionChecker(cc)
if setviewer:
    env.SetViewer(viewername)
    vw = env.GetViewer()

## Robots
robotpath = path.expanduser('~') + '/git/bimanual/xml/robots/'
robotfilename = 'denso_ft_gripper_with_base.robot.xml'
manipulatorname = 'denso_ft_sensor_gripper'
lrobot = env.ReadRobotXMLFile(robotpath + robotfilename)
rrobot = env.ReadRobotXMLFile(robotpath + robotfilename)
lrobot.SetName('lrobot')
rrobot.SetName('rrobot')
env.Add(lrobot)
env.Add(rrobot)
lrobot.SetActiveDOFs(xrange(6))
rrobot.SetActiveDOFs(xrange(6))

Tleft = utils.ComputeTRot(2, np.pi/2)
Tright = utils.ComputeTRot(2, -np.pi/2)
p = np.array([0.003, 1.078, 0.002])
Tright[0:3, 3] += p
lrobot.SetTransform(Tleft)
rrobot.SetTransform(Tright)

# Supporting surface
modelspath = '../pymanip/models/'
supporting_surface = env.ReadKinBodyXMLFile(modelspath + 'supporting_surface.kinbody.xml')
env.Add(supporting_surface)
Trest = np.array([[ 1.,  0.,  0.,  0.5*p[0]],
                  [ 0.,  1.,  0.,  0.5*p[1]],
                  [ 0.,  0.,  1., -0.155],
                  [ 0.,  0.,  0.,  1.]])
supporting_surface.SetTransform(Trest)
# surface_frame = utils.CreateReferenceFrameKinBody(env, T=Trest, h=0.2, r=0.005)

## Setting up manipulators
ik6d = orpy.IkParameterization.Type.Transform6D
lmanip = lrobot.SetActiveManipulator(manipulatorname)
rmanip = rrobot.SetActiveManipulator(manipulatorname)
ltaskmanip = orpy.interfaces.TaskManipulation(lrobot)
rtaskmanip = orpy.interfaces.TaskManipulation(rrobot)
lcontroller = lrobot.GetController()
rcontroller = rrobot.GetController()
ik6d = orpy.IkParameterization.Type.Transform6D
likmodel = orpy.databases.inversekinematics.InverseKinematicsModel(lrobot, iktype=ik6d)
if not likmodel.load():
    raise Exception("Cannot load ikmodel for left-robot")
rikmodel = orpy.databases.inversekinematics.InverseKinematicsModel(rrobot, iktype=ik6d)
if not rikmodel.load():
    raise Exception("Cannot load ikmodel for right-robot")

##########################################################################################
## Certificate issuer
from pymanip.planners import issuer

availableObjects = dict()
availableObjects[len(availableObjects.keys())] = "chair.xml"
availableObjects[len(availableObjects.keys())] = "ikea-stefan.xml"
availableObjects[len(availableObjects.keys())] = "taburet1.xml"
availableObjects[len(availableObjects.keys())] = "taburet2.xml"
availableObjects[len(availableObjects.keys())] = "square.xml"
availableObjects[len(availableObjects.keys())] = "praktrik3.xml"
availableObjects[len(availableObjects.keys())] = "forte.xml"
msg = "Choose an object to be manipulated:"
for (key, val) in availableObjects.iteritems():
    msg += "\n{0} : {1}".format(key, val)
msg += "\n\n>> "
while True:
    try:
        index = int(raw_input(msg))
        if index in availableObjects.keys():
            break
    except:
        pass        

## Movable object
objectname = availableObjects[index]
mobj = env.ReadKinBodyXMLFile(modelspath + objectname)
env.Add(mobj)

## Initializing MyObject
myObject = myobject.MyObject(mobj, autoLoad=0)
myObject.SetRestingSurfaceTransform(Trest)

## Obtain a dictionary for saving data
filename = "issuer-stats_" + path.splitext(objectname)[0] + ".pp"
print "filename for saving stats is {0}".format(filename)
if path.isfile(filename):
    with open(filename, 'rb') as f:
        stats = pickle.load(f)
else:
    stats = dict()

if len(stats.keys()) == 0:
    stats = dict()
    # Now initialize some entries
    stats['totaltime']        = []
    stats['planningtime']     = [] # time spent planning successful queries
    stats['eqcheckingtime']   = [] # time spent checking grasp eq
    stats['shortcuttime']     = [] # time spent for shortcutting
    stats['results']          = [] # 1 if successful, 0 otherwise
    stats['ntransfers']       = [] # total number of cctrajs planned
    stats['nCCPlannerFailed'] = [] # total number of failed queries
    stats['nGraspEqFailed']   = [] # total number of failed grasp eq checking trajs
    
    ## Collect basic information of the object
    nplacements = len(myObject.stablefacetindices)
    ngrasps = 0
    for g in myObject.graspinfos.values():
        allApproachingDirs = []
        for vals in g.approachingDirections.values():
            allApproachingDirs += vals
        ngrasps += len(list(set(allApproachingDirs)))
    ngrasps **= 2 # we consider bimanual grasp classes

    stats['nplacements'] = nplacements
    stats['ngrasps'] = ngrasps
    
n = 50 # number of times to collect stats
passed = False
itrial = 0

while not passed:
    planner = issuer.Issuer([lrobot, rrobot], mobj, myObject)
    ts = time.time()
    result = planner.ComputeCertificate(np.array([0.25, 0]), True)
    te = time.time()

    # Record data
    stats['totaltime'].append(te - ts)
    stats['results'].append(int(result))
    stats['ntransfers'].append(len(planner.placementconnection.edges()))
    edges = planner.placementconnection.edges(data=True)
    planning = 0.0
    shortcutting = 0.0
    checking = 0.0
    nplanfailed = 0
    ngraspfailed = 0
    for (v0, v1, data) in edges:
        planning += data['planningtime']
        shortcutting += data['shortcuttime']
        checking += data['eqcheckingtime']
        nplanfailed += data['nCCPlannerFailed']
        ngraspfailed += data['nGraspEqFailed']
    stats['planningtime'].append(planning)
    stats['shortcuttime'].append(shortcutting)
    stats['eqcheckingtime'].append(checking)
    stats['nCCPlannerFailed'].append(nplanfailed)
    stats['nGraspEqFailed'].append(ngraspfailed)

    itrial += 1
    if sum(stats['results']) == n:
        passed = True
        
    with open(filename, 'wb') as f:
        pickle.dump(stats, f, pickle.HIGHEST_PROTOCOL)
    
    print "\n\n"
    print "Finish trial {0}\nrunning time = {1} s.".format(itrial, te - ts)
    print "Stats saved to {0}".format(filename)
    print "\n\n"

"""
# Result reporting (for putting in the paper)

with open(filename, 'r') as f:
    stats = pickle.load(f)
msg = "${0}$ & ${1}$ & ${2:.02f}$ & ${3:.02f}$ & ${4:.02f}$ & ${5:.02f}$ & ${6:.02f}$".\
      format(stats['nplacements'], stats['ngrasps'], np.average(stats['totaltime']),
             np.std(stats['totaltime']), np.average(stats['planningtime']) +
             np.average(stats['eqcheckingtime']), np.average(stats['shortcuttime']),
             np.average(stats['ntransfers']))
print msg
"""
