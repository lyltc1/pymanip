import openravepy as orpy
import numpy as np
env = orpy.Environment()
env.SetViewer('qtcoin')
vw = env.GetViewer()

test1 = orpy.RaveCreateKinBody(env, '')

testgi = orpy.GeometryInfo()
testgi._type = orpy.GeometryType.Cylinder
r = 0.2
h = 0.02
testgi._vGeomData = np.array([r, h, 0, 0])
testgi._vDiffuseColor = np.array([1, 0.5, 0])

test1.InitFromGeometries([testgi])
test1.SetName('test1')

env.Add(test1)

"""
From observation:

geometryInfo._vGeomData is a 4-vector
if the type is box, then the first three elements are box extents
if the type is cylinder, then the first element is the radius and the second one is the height
"""
