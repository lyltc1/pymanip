pymanip/tests
=============

* `statscollector.py`   : for running the simulations whose data is presented in the paper

* `test_bquerysolver.py`: for testing bimanual query sovler

* `test_issuer.py`      : for testing certificate computation with various objects

* `test_placement_connectedness.py`: for collecting data (and plotting) of placement connectedness

* `trajgeneration.py`   : for generating the trajectory presented in the paper.
