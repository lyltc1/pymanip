import openravepy as orpy
import numpy as np
import time
import IPython
import random
rng = random.SystemRandom()

from bimanual.planners import cc_planner as ccp
from bimanual.utils.trajectory import SE3Trajectory, CCTrajectory
from bimanual.utils import lie
from pymanip.planningutils import utils
from pymanip.planningutils import myobject
from bimanual.utils.misc_planning import plan_transit_motion
from pymanip.planningutils.trajectory import TransitTrajectory, ManipulationTrajectory

from TOPP import Trajectory

home = np.zeros(6)

"""
Generation of manipulation trajectory for executing on the real system
"""

## Environment initialization
viewername = 'qtosg'
collisioncheckername = 'fcl_'
env = orpy.Environment()
env.SetViewer(viewername)
vw = env.GetViewer()
cc = orpy.RaveCreateCollisionChecker(env, collisioncheckername)
cc.SetCollisionOptions(orpy.CollisionOptions.ActiveDOFs)
env.SetCollisionChecker(cc)

# Robots
from os import path
robotpath = path.expanduser('~') + '/git/bimanual/xml/robots/'
robotfilename = 'denso_ft_gripper_with_base.robot.xml'
manipulatorname = 'denso_ft_sensor_gripper'
lrobot = env.ReadRobotXMLFile(robotpath + robotfilename)
rrobot = env.ReadRobotXMLFile(robotpath + robotfilename)
lrobot.SetName('lrobot')
rrobot.SetName('rrobot')
env.Add(lrobot)
env.Add(rrobot)
lrobot.SetActiveDOFs(xrange(6))
rrobot.SetActiveDOFs(xrange(6))

Tleft = utils.ComputeTRot(2, np.pi/2)
Tright = utils.ComputeTRot(2, -np.pi/2)
p = np.array([0.003, 1.078, 0.002])
Tright[0:3, 3] += p
lrobot.SetTransform(Tleft)
rrobot.SetTransform(Tright)

# Supporting surface
modelspath = '../pymanip/models/'
supporting_surface = env.ReadKinBodyXMLFile(modelspath + 'supporting_surface2.kinbody.xml')
env.Add(supporting_surface)
Trest = np.array([[ 1.,  0.,  0.,  0.5*p[0]],
                  [ 0.,  1.,  0.,  0.5*p[1]],
                  [ 0.,  0.,  1., -0.155],
                  [ 0.,  0.,  0.,  1.]])
Trest[2, 3] = -0.057 # Due to some unexpected circumstances, the support surface is moved
                     # to 5.7 cm. below the left robot's level
supporting_surface.SetTransform(Trest)
surface_frame = utils.CreateReferenceFrameKinBody(env, T=Trest, h=0.2, r=0.005)

# Movable object
mobj = env.ReadKinBodyXMLFile(modelspath + "ikea-stefan.xml")
env.Add(mobj)

## Setting up manipulators
ik6d = orpy.IkParameterization.Type.Transform6D
lmanip = lrobot.SetActiveManipulator(manipulatorname)
rmanip = rrobot.SetActiveManipulator(manipulatorname)
ltaskmanip = orpy.interfaces.TaskManipulation(lrobot)
rtaskmanip = orpy.interfaces.TaskManipulation(rrobot)
lcontroller = lrobot.GetController()
rcontroller = rrobot.GetController()
ik6d = orpy.IkParameterization.Type.Transform6D
likmodel = orpy.databases.inversekinematics.InverseKinematicsModel(lrobot, iktype=ik6d)
if not likmodel.load():
    raise Exception("Cannot load ikmodel for left-robot")
rikmodel = orpy.databases.inversekinematics.InverseKinematicsModel(rrobot, iktype=ik6d)
if not rikmodel.load():
    raise Exception("Cannot load ikmodel for right-robot")

## Initializing MyObject
myObject = myobject.MyObject(mobj)
myObject.SetRestingSurfaceTransform(Trest)

##########################################################################################
# Add padding to robot bases
def box(dx, dy, dz, transparency=0.4, scale=1.0):
    orbox = orpy.RaveCreateKinBody(env, '')
    orbox.InitFromBoxes(np.array([[0.0, 0.0, 0.0, dx*scale, dy*scale, dz*scale]]))
    orbox.SetName('b')
    env.Add(orbox, True) # allow name clash (fixing it by appending numbers to the name)
    for l in orbox.GetLinks():
        for g in l.GetGeometries():
            g.SetTransparency(transparency)
    return orbox

b1 = box(0.01, 0.25, 0.05)
Tb1 = np.eye(4)
Tb1[0:3, 3] = np.array([0.2601, 0, -0.04])
b1.SetTransform(Tb1)

b2 = box(0.27, 0.01, 0.05)
Tb2 = np.eye(4)
Tb2[0:3, 3] = np.array([0, 0.2601, -0.04])
b2.SetTransform(Tb2)

b3 = box(0.01, 0.25, 0.05)
Tb3 = np.eye(4)
Tb3[0:3, 3] = np.array([0.2601, 0, -0.04]) + p
b3.SetTransform(Tb3)

b4 = box(0.27, 0.01, 0.05)
Tb4 = np.eye(4)
Tb4[0:3, 3] = np.array([0, -0.2601, -0.04]) + p
b4.SetTransform(Tb4)
##########################################################################################
import pickle
from pymanip.planners import bquerysolver as bs
from pymanip.planningutils.utils import OpenRAVEKinBodyWrapper
filename = 'data/certificate_ikea-stefan.updated-surface.pp'

loadCertFromFile = True
if not loadCertFromFile:
    from pymanip.planners import issuer
    planner = issuer.Issuer([lrobot, rrobot], mobj, myObject)
    ts = time.time()
    result = planner.ComputeCertificate(np.array([0.25, 0]), True)
    te = time.time()
    print "Verification result: {0}".format('passed' if result else 'not passed')
    print "verification time = {0}".format(te - ts)
    placementConnections = planner.placementconnection.edges(data=True)
else:
    with open(filename, 'r') as f:
        placementConnections = pickle.load(f)

paddedObject = env.ReadKinBodyXMLFile(modelspath + "ikea-stefan.padded.xml")
env.Add(paddedObject, True)

orwrapper = OpenRAVEKinBodyWrapper(mobj, myObject.Tcom)
solver = bs.BQuerySolver([lrobot, rrobot], orwrapper, myObject, placementConnections, paddedObject)
paddedwrapper = solver.se2planner.paddedOrwrapper

# qobj_start = [210, -0.036219546320382781, 0.037202693046315988, 1.8038674769996976]
# qobj_start = [210, 0, 0, np.pi/2]
qobj_start = [210, 0.235, 0, np.pi/2]
qobj_goal = [28, 0.15073634536689201, 0.0097355158857016823, -2.5654131525883996]
Tstart = myObject.ComputeTObject(qobj_start)
Tgoal = myObject.ComputeTObject(qobj_goal)
bquery = bs.BQuery(Tstart, Tgoal)

import IPython; IPython.embed(header="Finished initialization")

##########################################################################################
# Auto generation (works only with lots of luck)
manipTraj = solver.Solve(bquery)

##########################################################################################
# Try manual generation (for more control)
import networkx as nx
if bquery.Ttype == 'object':
    TcomStart = np.dot(bquery.Tstart, myObject.Tcom)
    TcomGoal = np.dot(bquery.Tgoal, myObject.Tcom)
else: # Ttype == 'com'
    TcomStart = bquery.Tstart
    TcomGoal = bquery.Tstart
qobjStart = myObject.ComputeQObject(TcomStart, forRealObject=False)
qobjGoal = myObject.ComputeQObject(TcomGoal, forRealObject=False)

placementStart = qobjStart[0]
placementGoal = qobjGoal[0]

pc = nx.Graph()
for e in placementConnections:
    pc.add_edge(*e)
placementSequence = nx.shortest_path(pc, source=placementStart, target=placementGoal)

# Fetch data from the certificate
cctrajs = []
for i in xrange(len(placementSequence) - 1):
    pair = (placementSequence[i], placementSequence[i + 1])
    _reversed = False
    if pair not in pc.edges():
        assert(pair[::-1] in pc.edges())
        _reversed = True

    data = pc.get_edge_data(*pair)
    ccquery = data['certificate']
    if _reversed:
        cctraj = ccquery.cctraj.reverse()
    else:
        cctraj = ccquery.cctraj
    cctrajs.append(cctraj)

# Plan necessary in-placement transfer trajectories
ok = False
manipTrajs = []
while not ok:
    manipTraj = solver.se2planner.Solve(TcomStart, cctrajs[0].se3_traj.Eval(0), Ttype='com')
    while True:
        raw_input("Press any key to visualize the traj")
        utils.VisualizeManipulationTrajectory(manipTraj, lrobot, rrobot, orwrapper)
        res = raw_input("How do you think? [a = accept; r = replan; else = visualize]")
        if res == 'r':
            break
        elif res == 'a':
            ok = True
            break    
manipTrajs.append(manipTraj)

i = 0
while i < len(cctrajs) - 1:
    ok = False
    while not ok:
        se3traj = cctrajs[i].se3_traj
        nextse3traj = cctrajs[i + 1].se3_traj
        manipTraj = solver.se2planner.Solve(se3traj.Eval(se3traj.duration),
                                            nextse3traj.Eval(0), Ttype='com')
        while True:
            raw_input("Press any key to visualize the traj")
            utils.VisualizeManipulationTrajectory(manipTraj, lrobot, rrobot, orwrapper)
            res = raw_input("How do you think? [a = accept; r = replan; else = visualize]")
            if res == 'r':
                break
            elif res == 'a':
                ok = True
                break
    manipTrajs.append(manipTraj)
    i += 1

ok = False
while not ok:
    se3traj = cctrajs[-1].se3_traj
    manipTraj = solver.se2planner.Solve(se3traj.Eval(se3traj.duration), TcomGoal, Ttype='com')
    while True:
        raw_input("Press any key to visualize the traj")
        utils.VisualizeManipulationTrajectory(manipTraj, lrobot, rrobot, orwrapper)
        res = raw_input("How do you think? [a = accept; r = replan; else = visualize]")
        if res == 'r':
            break
        elif res == 'a':
            ok = True
            break
manipTrajs.append(manipTraj)

# Save manip trajectories
manipTrajs[0].Save('maniptraj0.pp')
manipTrajs[1].Save('maniptraj1.pp')
manipTrajs[2].Save('maniptraj2.pp')


##########################################################################################
# Bridging gaps and constructing the finalized trajectory
finalTraj = ManipulationTrajectory()
TRANSIT = ManipulationTrajectory.TRANSIT
TRANSFER = ManipulationTrajectory.TRANSFER

config_epsilon = 1e-6

# This is how the final sequence looks like:
# transitTraj1 --> manipTrajs[0] --> transitTraj2 --> cctrajs[0] --> transitTraj3 -- >
# manipTrajs[1] --> transitTraj4 --> cctrajs[1] --> transitTraj5 -- > manipTrajs[2] --> transitTraj6

def Bridge(cctraj1=None, cctraj2=None, ql_start=None, qr_start=None, ql_goal=None, qr_goal=None, pgstart=True, pggoal=True):
    config_epsilon = 1e-6
    if cctraj1 is None:
        assert(ql_start is not None)
        assert(qr_start is not None)
    else:
        ql_start = cctraj1.bimanual_wpts[0][-1]
        qr_start = cctraj1.bimanual_wpts[1][-1]

    if cctraj2 is None:
        assert(ql_goal is not None)
        assert(qr_goal is not None)
    else:
        ql_goal = cctraj2.bimanual_wpts[0][0]
        qr_goal = cctraj2.bimanual_wpts[1][0]

    if cctraj1 is None:
        Tobj = cctraj2.se3_traj.Eval(0)
    else:
        Tobj = cctraj1.se3_traj.Eval(cctraj1.se3_traj.duration)
        
    orwrapper.SetTransform(Tobj)
    paddedwrapper.SetTransform(Tobj)
    solver.se2planner.EnablePadded(True)
    
    rrobot.SetActiveDOFValues(qr_start)
    diff = ql_start - ql_goal
    if np.dot(diff, diff) >= config_epsilon:
        lTraj = plan_transit_motion(lrobot, ql_start, ql_goal, pregrasp_start=pgstart, pregrasp_goal=pggoal)
        assert(lTraj is not None)
    lrobot.SetActiveDOFValues(ql_goal)
    diff = qr_start - qr_goal
    if np.dot(diff, diff) >= config_epsilon:
        rTraj = plan_transit_motion(rrobot, qr_start, qr_goal, pregrasp_start=pgstart, pregrasp_goal=pggoal)
        assert(rTraj is not None)
    transitTraj = TransitTrajectory(lTraj, rTraj)

    solver.se2planner.EnablePadded(False)
    return transitTraj

## transit traj 1
tt1 = Bridge(cctraj2=manipTrajs[0][0], ql_start=home, qr_start=home, pgstart=False, pggoal=True)
finalTraj.AddTrajectory(tt1, TRANSIT)
finalTraj.AddManipTrajectory(manipTrajs[0])

## transit traj 2
tt2 = Bridge(cctraj1=manipTrajs[0][-1], cctraj2=cctrajs[0], ql_start=None, qr_start=None, pgstart=True, pggoal=True)
finalTraj.AddTrajectory(tt2, TRANSIT)
finalTraj.AddTrajectory(cctrajs[0], TRANSFER)

## transit traj 3
tt3 = Bridge(cctraj1=cctrajs[0], cctraj2=manipTrajs[1][0], ql_start=None, qr_start=None, pgstart=True, pggoal=True)
finalTraj.AddTrajectory(tt3, TRANSIT)
finalTraj.AddManipTrajectory(manipTrajs[1])

## transit traj 4
tt4 = Bridge(cctraj1=manipTrajs[1][-1], cctraj2=cctrajs[1], ql_start=None, qr_start=None, pgstart=True, pggoal=True)
finalTraj.AddTrajectory(tt4, TRANSIT)
finalTraj.AddTrajectory(cctrajs[1], TRANSFER)

## transit traj 5
tt5 = Bridge(cctraj1=cctrajs[1], cctraj2=manipTrajs[2][0], ql_start=None, qr_start=None, pgstart=True, pggoal=True)
finalTraj.AddTrajectory(tt5, TRANSIT)
finalTraj.AddManipTrajectory(manipTrajs[2])

## transit traj 6
tt6 = Bridge(cctraj1=manipTrajs[2][-1], ql_goal=home, qr_goal=home, pgstart=True, pggoal=False)
finalTraj.AddTrajectory(tt6, TRANSIT)

##########################################################################################

def TimeScaleCCTrajectory(cctraj, tmult):
    """Suppose the original cctraj is parameterized from t = 0 to t = T. This function will
    return a new cctraj whose path parameter is s, s \in [0, tmult * T].

    For timestamps for bimanual waypoints, each time stamp is directly multiplied by tmult.

    """

    se3_traj = cctraj.se3_traj
    new_trans_traj = Trajectory.ScaleTrajectory(se3_traj.translation_traj, tmult)

    Rlist = se3_traj.lie_traj.Rlist
    newtrajlist = []
    for traj in se3_traj.lie_traj.trajlist:
        newtraj = Trajectory.ScaleTrajectory(traj, tmult)
        newtrajlist.append(newtraj)
    new_lie_traj = lie.LieTraj(Rlist, newtrajlist)

    new_se3_traj = SE3Trajectory(new_lie_traj, new_trans_traj)
    
    new_timestamps = [t * tmult for t in cctraj.timestamps]
    new_timestep = cctraj.timestep * tmult

    new_cctraj = CCTrajectory(new_se3_traj, cctraj.bimanual_wpts, new_timestamps, new_timestep)
    return new_cctraj

vmult = 0.15
tmult = 5.0
vmax = lrobot.GetDOFVelocityLimits()

ntrajs = len(finalTraj.trajsList)
orwrapper.SetTransform(finalTraj[1].se3_traj.Eval(0))
for itraj, (traj, trajtype) in enumerate(zip(finalTraj.trajsList, finalTraj.trajTypesList)):
    print "itraj = {0}".format(itraj)
    if trajtype == ManipulationTrajectory.TRANSFER:
        newcctraj = TimeScaleCCTrajectory(traj, tmult)
        finalTraj.trajsList[itraj] = newcctraj
        orwrapper.SetTransform(traj.se3_traj.Eval(traj.se3_traj.duration))
        paddedwrapper.SetTransform(traj.se3_traj.Eval(traj.se3_traj.duration))
        lrobot.SetActiveDOFValues(traj.bimanual_wpts[0][-1])
        rrobot.SetActiveDOFValues(traj.bimanual_wpts[1][-1])
        
    else: # TRANSIT
        solver.se2planner.EnablePadded(True)
        
        lrobot.SetDOFVelocityLimits(vmax * vmult)
        rrobot.SetDOFVelocityLimits(vmax * vmult)
        ps = True
        pg = True
        if itraj == 0:
            ps = False
        elif itraj == ntrajs - 1:
            pg = False
        # Retime the trajectories with lower velocity limits        
        if traj.lTraj is not None:
            lspec = traj.lTraj.GetConfigurationSpecification()
            ljgroup = lspec.GetGroupFromName('joint_values {0}'.format(lrobot.GetName()))
            ql_start = traj.lTraj.GetWaypoint(0)[ljgroup.offset: ljgroup.offset + 6]
            ql_goal = traj.lTraj.GetWaypoint(traj.lTraj.GetNumWaypoints() - 1)[ljgroup.offset: ljgroup.offset + 6]
            lTraj = plan_transit_motion(lrobot, ql_start, ql_goal, pregrasp_start=ps, pregrasp_goal=pg)
            assert(lTraj is not None)
            traj.lTraj = lTraj
        if traj.rTraj is not None:
            rspec = traj.rTraj.GetConfigurationSpecification()
            rjgroup = rspec.GetGroupFromName('joint_values {0}'.format(rrobot.GetName()))
            qr_start = traj.rTraj.GetWaypoint(0)[rjgroup.offset: rjgroup.offset + 6]
            qr_goal = traj.rTraj.GetWaypoint(traj.rTraj.GetNumWaypoints() - 1)[rjgroup.offset: rjgroup.offset + 6]
            rTraj = plan_transit_motion(rrobot, qr_start, qr_goal, pregrasp_start=ps, pregrasp_goal=pg)
            assert(rTraj is not None)
            traj.rTraj = rTraj
        lrobot.SetDOFVelocityLimits(vmax)
        rrobot.SetDOFVelocityLimits(vmax)

        solver.se2planner.EnablePadded(False)

##########################################################################################
def SpecialVisualization(manipTraj, lrobot, rrobot, orwrapper):
    for itraj, (traj, trajtype) in enumerate(zip(manipTraj.trajsList, manipTraj.trajTypesList)):
        m = ManipulationTrajectory([traj], [trajtype])
        print "itraj {0}".format(itraj)
        utils.VisualizeManipulationTrajectory(m, lrobot, rrobot, orwrapper, timestep=0.008)
        raw_input("Press any key to execute the next trajectory (if any)")
